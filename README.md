# Squelette docusaurus à s'approprier pour monter une documentation Docusaurus rapide 

Pour contourner les problèmes de blocage des postes académiques qui n’ont pas les applications nécessaires pour travailler en local et monter un site docusaurus distant, "Forker (bifurcation)" ce dépôt distant "squelette Docusaurus" pour vous l’approprier.

- Pour la documentation :   
*pour copier le dépôt distant, customiser son docusaurus, éditer ses pages en markdown ...*  
Passez par la documentation du site squelette ''Menu DOCS à modifier'' 

### Voir le rendu du site squelette à disposition à l'adresse ci-dessous :

https://squelettedocusaurus-eric-autant-c62fb0c653ae143c497c34ff76b1f39.forge.apps.education.fr/
